import React from 'react';
import { Link, Router, Location } from '@reach/router'

const Note = props => (
  <div>
    <Location>
      {({location}) => (
        <p>{(new URLSearchParams(location.search)).get('title')}</p>
      )}
    </Location>
  </div>
)

const App = () => (
  <div className="App w-full max-w-md bg-gray-800" >
    <h1>Tutorial!</h1>
    <nav>
      <Link to="notes/123?title=OneTwoThree">Note 123</Link>{" "}
      <Link to="notes/abc?title=ABC">Note ABC</Link>
    </nav>
    
    <Router>
      <Note path="notes/:NoteId" />
    </Router>
  </div>
);

export default App;
